PKG_CONFIG_PATH="/usr/local/opt/libffi/lib/pkgconfig"

main: main.c event.c
	gcc -o $@ $< `PKG_CONFIG_PATH=$(PKG_CONFIG_PATH) pkg-config gtk+-3.0 --cflags --libs`

event.c: extract.pl
	perl extract.pl > $@
