#!/usr/bin/perl

$filepath = "/usr/local/include/gtk-3.0/gdk/gdkevents.h";

$inside = 0;
open(EVENT, $filepath);
while(<EVENT>) {
 $inside = 1 if /GDK_NOTHING/;
 $inside = 0 if /GDK_EVENT_LAST/;

 next if /DOUBLE/;
 next if /TRIPLE/;
 next if /GDK_EVENT_LAST/;

 if ($inside == 1 && !/\*/) {
   /\w+/;
   $word = $&;
   print<<"EOF"
  case $word:
    g_print("$word\\n");
    break;
EOF
 }
}
close(EVENT);
