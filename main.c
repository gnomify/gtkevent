#include <gtk/gtk.h>

void
on_event(GtkWidget* widget, GdkEvent *event)
{
  switch (event->type) {
#include "event.c"
  }
}

int
main(int argc, char** argv)
{
  GtkWidget* window;
  GError *error = NULL;

  gtk_init(&argc, &argv);

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);

  gtk_widget_show_all (window);
  gtk_widget_add_events (window, GDK_ALL_EVENTS_MASK);
    g_signal_connect(G_OBJECT(window), "event", 
      G_CALLBACK(on_event), NULL);

  gtk_main ();

  return 0;
}
